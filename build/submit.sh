#!/bin/bash
#SBATCH --job-name="hgraph"
#SBATCH --output="hgraph.%j.%N.out"
#SBATCH --partition=compute
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=10
#SBATCH -t 00:30:00

nodes=$((22))
cores=$((10))
numv=100000
nume=3162277
#base=501187
#base=316228
#base=223782
base=199525
#base=281838
#base=158489
#base=1122019
alg=1

# java -XX:+PrintFlagsFinal -version | grep -iE 'HeapSize|PermSize|ThreadStackSize'


export HADOOP_CONF_DIR=/home/$USER/cometcluster
export WORKDIR=`pwd`
module load hadoop
myhadoop-configure.sh
export PATH=/opt/hadoop/2.6.0/sbin:$PATH
start-all.sh
# hdfs dfs -mkdir -p /user/$USER/input
# hdfs dfs -put $WORKDIR/SINGLE.TXT /user/$USER/input/SINGLE.TXT
# hadoop jar $WORKDIR/AnagramJob.jar /user/$USER/input/SINGLE.TXT /user/$USER/output
# hdfs dfs -get /user/$USER/output/part* $WORKDIR/


hdfs dfs -mkdir -p /user/$USER/input/input.0


echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores node $numv edges $nume $base "

hadoop jar ../target/disalgorithms-1.0-SNAPSHOT.jar org.hadoopdisgraph.Main $alg $numv $nume $base /user/$USER/input/input.0/part-r-00000


stop-all.sh
myhadoop-cleanup.sh
