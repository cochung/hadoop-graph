#!/bin/bash
#SBATCH --job-name="hgraph"
#SBATCH --output="hgraph.%j.%N.out"
#SBATCH --partition=compute
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=10
#SBATCH -t 00:30:00




export HADOOP_CONF_DIR=/home/$USER/cometcluster
export WORKDIR=`pwd`
module load hadoop
myhadoop-configure.sh
export PATH=/opt/hadoop/2.6.0/sbin:$PATH
start-all.sh



alg=1
numV=100000
numE=3162277
path=$(hdfs getconf -confKey fs.defaultFS)
hdfs dfs -rm -r -f $path/user/$USER/*
hdfs dfs -mkdir $path/user/$USER/input
echo "sconfig:------------different base case with same core---------------"

base2=(1000000 501187 316228 223782 199525 177829 158489)
len2=${#base2[@]}

var=1
for(( i=0; i<${len2}; i++ ));
do
echo "debuginfo: algorithm: $alg  nodes:$nodes cores:$cores assignd_cores: $j node $numV edges $numE base ${base2[$i]} "

hadoop jar ../target/disalgorithms-1.0-SNAPSHOT.jar org.hadoopdisgraph.Main \
$alg $numV $numE ${base2[$i]} $path/user/$USER/input/input.$var

echo "variable: "$var
var=$((var + 1))


echo "******************************************"
done



stop-all.sh
myhadoop-cleanup.sh
