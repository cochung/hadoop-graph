package org.hadoopdisgraph;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.conf.Configuration;
import java.util.HashSet;
import org.apache.hadoop.util.GenericOptionsParser;

import java.lang.System;

public class Main extends Configured {
	public static void main(String[] args) {

		try {
			new Main().run(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int run(String[] args) throws Exception {

		Configuration conf = new Configuration();
		GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);

		System.out.println(conf.toString());
		System.out.println("parameters: ");
		for (String str : args) {
			System.out.println(str);
		}

		int src = Integer.valueOf(args[0]);
		int numV = Integer.valueOf(args[1]);
		long numE = Long.valueOf(args[2]);
		int base = Integer.valueOf(args[3]);
		String fileName = args[4];
		if (src == 1) {

			Test.initMaximalTestData(conf, numV, numE, base, fileName);

			long start = System.currentTimeMillis();
			DistributedMaximalMatching mm = new DistributedMaximalMatching(conf, numV, numE, base, fileName);
			List<Edge> matchedEdges = mm.getMatchedEdges();
			HashSet<Integer> matchedIndex = mm.getMatchedIndex();
			System.out.println("matchedEdgeSize " + matchedEdges.size());
			System.out.println("matchedIndexSize " + matchedIndex.size());

			long end = System.currentTimeMillis();
			ElapsedTime.logElapsedTime(start, end, "maximalMatching");

		} else if (src == 2) {

			Test.initMSTTestData(conf, numV, numE, base, fileName);

			long start = System.currentTimeMillis();
			DistributedMST disMST = new DistributedMST(conf, numV, numE, base, fileName);
			List<Edge> mstList = disMST.getMstList();
			System.out.println("mstListSize: " + mstList.size());

			long end = System.currentTimeMillis();
			ElapsedTime.logElapsedTime(start, end, "MST");

		}

		else if (src == 3) {

			fileName = Config.getProperty("testData");
			Test.initMSTTestData(conf, numV, numE, base, fileName);

			long start = System.currentTimeMillis();
			DistributedMST disMST = new DistributedMST(conf, numV, numE, base, fileName);
			List<Edge> mstList = disMST.getMstList();
			// System.out.println("mstListSize: " + mstList.size());

			UnionFindSet ufset = new UnionFindSet();
			HashMap<Integer, ArrayList<Integer>> ccmap = ufset.getConnectedComponent(mstList);
			System.out.println("ccmap size: " + ccmap.size());
			// ccmap.keySet().forEach(System.out::println);
			long end = System.currentTimeMillis();
			ElapsedTime.logElapsedTime(start, end, "MST");
		}
		return 1;

	}
}
