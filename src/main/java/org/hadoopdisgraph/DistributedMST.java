package org.hadoopdisgraph;

import java.util.List;
import org.apache.hadoop.conf.Configuration;

public class DistributedMST {

    private List<Edge> mstList;
    private String prefix;
    private int job_count;

    public List<Edge> getMstList() {
        return this.mstList;
    }

    public DistributedMST(Configuration conf, int numV, long numE, int base, String input) {

        String prime = Config.getProperty("prime");
        conf.set("prime", prime);
        this.mstList = getMergedMST(conf, numV, numE, base, input);

    }

    private List<Edge> getMergedMST(Configuration conf, int numV, long numE, int base, String input) {
        prefix = input.split("\\.")[0];

        while (true) {
            String output = prefix + "." + job_count + ".count";
            long num = JobEdgeCount.runWrap(conf, input, output);
            job_count++;
            System.out.println("num:        " + num);
            if (num <= base) {

                List<String> edgeList = HdfsUtil.readFile(conf, input);
                // List<String> edgeList = FileIO.readFromFolder(input);
                List<Edge> colEdges = Edge.valueOf(edgeList);
                Kruskal kruskal = new Kruskal(colEdges);
                List<Edge> mst = kruskal.getMST();
                return mst;

            } else {
                System.out.println("divide!!!!!!!!!!!!");

                output = prefix + "." + job_count + ".partitioner";

                long pnum = num / (long) base + 1;
                conf.set("numReduce", String.valueOf(pnum));

                JobEdgePartitioner.run(conf, input, output);
                input = output;
                job_count++;

            }
        }
    }

}
