package org.hadoopdisgraph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper;

public class JobEdgePartitioner {

    public static class EdgeMapper extends Mapper<LongWritable, Text, IntWritable, Text> {

        static Random rand;
        {
            rand = new Random(System.currentTimeMillis());
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String str = value.toString();
            String[] edge = str.split(" ");
            long x = Long.valueOf(edge[0]);

            Configuration conf = context.getConfiguration();
            Integer p = Integer.valueOf(conf.get("prime"));
            Integer numReduce = Integer.valueOf(conf.get("numReduce"));

            long a = rand.nextInt(p - 1) + 1;
            long b = rand.nextInt(p - 1) + 1;

            long tmpS = ((a * x + b) % p);
            long u = tmpS % numReduce;
            int num = (int) u;

            context.write(new IntWritable(num), value);
        }
    }

    /**
     * Each partition processed by different reducer tasks as defined in our custom partitioner
     */
    public static class EdgeReducer extends Reducer<IntWritable, Text, NullWritable, Text> {

        NullWritable out = NullWritable.get();

        @Override
        public void reduce(IntWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            List<Edge> edges = new ArrayList<Edge>();
            edges = Edge.valueOf(values);

            Kruskal kruskal = new Kruskal(edges);
            List<Edge> mst = kruskal.getMST();

            for (Edge e : mst) {
                context.write(out, new Text(e.toString2()));
            }

        }

    }

    public static class customPartitioner extends Partitioner<IntWritable, Text> {
        public int getPartition(IntWritable key, Text value, int numReduceTasks) {
            int x = key.get();

            return x;

        }
    }

    public static int run(Configuration conf, String input, String output) {
        System.out.println("JobEdgePartitioner  " + input + "    " + output);

        try {
            long start = System.currentTimeMillis();

            Job job = new Job(conf);
            job.setJarByClass(JobEdgePartitioner.class);
            job.setJobName("JobEdgePartitioner");
            Integer numReduce = Integer.valueOf(conf.get("numReduce"));
            //Set number of reducer tasks
            job.setNumReduceTasks(numReduce);

            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);

            job.setMapOutputKeyClass(IntWritable.class);
            job.setMapOutputValueClass(Text.class);

            job.setMapperClass(EdgeMapper.class);
            job.setReducerClass(EdgeReducer.class);

            job.setPartitionerClass(customPartitioner.class);
            job.setInputFormatClass(TextInputFormat.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            FileInputFormat.setInputPaths(job, new Path(input));
            FileOutputFormat.setOutputPath(job, new Path(output));

            int ret = job.waitForCompletion(true) ? 0 : 1;
            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "edgePartitioner");
            return ret;

        } catch (Exception e) {
            System.out.println(e);
            return 0;

        }
    }

}