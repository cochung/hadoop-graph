package org.hadoopdisgraph;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.conf.Configuration;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;

public class HdfsUtil {

    public static int lineCount(Configuration conf, String path) {
        try {

            FileSystem fs = FileSystem.get(conf);
            Path pt = new Path(path);

            FileStatus[] status = fs.listStatus(pt);

            int count = 0;

            for (FileStatus f : status) {
                // if(f.isDir()){
                FSDataInputStream inputStream = fs.open(f.getPath());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line = reader.readLine();

                while (line != null) {
                    count++;
                    line = reader.readLine();
                }

                if (reader != null) {
                    reader.close();
                }
                // }

            }

            return count;

        } catch (Exception e) {
            System.out.println(e);
            return -1;
        }
    }

    public static void removeFile(Configuration conf, String path) {
        try {

            FileSystem fs = FileSystem.get(conf);
            Path pt = new Path(path);
            fs.delete(pt, true);
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeFile(Configuration conf, String path, List<String> lines) {
        try {

            FileSystem fs = FileSystem.get(conf);
            Path pt = new Path(path);

            FSDataOutputStream recOutputWriter = fs.create(pt);

            for (String line : lines) {
                recOutputWriter.writeBytes(line + "\n");
            }

            if (recOutputWriter != null) {
                recOutputWriter.close();
            }
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static List<String> readFile(Configuration conf, String path) {

        System.out.println("readPath:   " + path);
        try {

            FileSystem fs = FileSystem.get(conf);
            Path pt = new Path(path);

            FileStatus[] status = fs.listStatus(pt);

            List<String> list = new ArrayList<String>();
            int count = 0;

            for (FileStatus f : status) {
                // if(f.isDir()){
                FSDataInputStream inputStream = fs.open(f.getPath());
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;

                while ((line = reader.readLine()) != null) {
                    count++;
                    list.add(line);
                }

                if (reader != null) {
                    reader.close();
                }
                // }

            }

            return list;

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

}