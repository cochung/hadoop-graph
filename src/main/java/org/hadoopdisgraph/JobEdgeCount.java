package org.hadoopdisgraph;

import java.util.*;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.fs.Path;

public class JobEdgeCount {
    public static class EdgeMapper extends Mapper<LongWritable, Text, Text, LongWritable> {
        LongWritable one = new LongWritable(1);

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            context.write(new Text("count"), one);
        }
    }

    public static class EdgeReducer extends Reducer<Text, LongWritable, NullWritable, LongWritable> {
        NullWritable out = NullWritable.get();
        long sum = 0;

        @Override
        protected void reduce(Text key, Iterable<LongWritable> values, Context context)
                throws IOException, InterruptedException {

            Iterator<LongWritable> valuesIt = values.iterator();
            while (valuesIt.hasNext()) {
                sum = sum + valuesIt.next().get();

            }

            context.write(out, new LongWritable(sum));

        }
    }

    public static int run(Configuration conf, String input, String output) {

        System.out.println("JobEdgeCount    " + input + "    " + output);

        try {

            long start = System.currentTimeMillis();

            Job job = new Job(conf);
            job.setJarByClass(JobEdgeCount.class);
            job.setJobName("JobEdgeCount");

            FileInputFormat.addInputPath(job, new Path(input));
            FileOutputFormat.setOutputPath(job, new Path(output));

            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(LongWritable.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            job.setMapperClass(JobEdgeCount.EdgeMapper.class);
            job.setReducerClass(JobEdgeCount.EdgeReducer.class);

            int ret = job.waitForCompletion(true) ? 0 : 1;

            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "edgeCount");

            return ret;
        } catch (Exception e) {
            System.out.println("error");
            e.printStackTrace();
            return 0;

        }
    }

    public static int runWrap(Configuration conf, String input, String output) {

        JobEdgeCount.run(conf, input, output);
        List<String> list = HdfsUtil.readFile(conf, output);
        if (list.size() == 0)
            return 0;
        else
            return Integer.valueOf(list.get(0));

    }

}