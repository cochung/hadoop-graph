package org.hadoopdisgraph;

import java.util.List;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import org.apache.commons.io.FileUtils;

public class FileIO {

    public static List<String> readFromFile(String str) {
        String line;
        BufferedReader bufferreader;
        List<String> list = new ArrayList<String>();
        try {

            bufferreader = new BufferedReader(new FileReader(str));
            line = bufferreader.readLine();
            while ((line = bufferreader.readLine()) != null) {
                list.add(line);
            }
            bufferreader.close();
            return list;

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();

            return null;
        } catch (IOException ex) {
            ex.printStackTrace();

            return null;
        }
    }

    public static List<String> readFromFolder(String folder) {

        String line;
        BufferedReader bufferreader;
        List<String> list = new ArrayList<String>();
        try {
            File dir = new File(folder);
            File[] listOfFiles = dir.listFiles();

            for (File file : listOfFiles) {

                if (file.isFile() && (file.getName().startsWith("part-r"))) {

                    bufferreader = new BufferedReader(new FileReader(file.getAbsolutePath()));
                    while ((line = bufferreader.readLine()) != null) {
                        list.add(line);
                    }
                    bufferreader.close();
                }

            }

            return list;

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();

            return null;
        } catch (IOException ex) {
            ex.printStackTrace();

            return null;
        }
    }

    public static void writeToFile(List<String> list, String str) {

        try {
            File fout = new File(str);
            BufferedWriter bw = new BufferedWriter(new FileWriter(fout));

            for (String line : list) {
                bw.write(line);
                bw.newLine();
            }
            ;
            bw.close();
        }

        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void removeFromFolder(String folder) {

        try {
            File dir = new File(folder);
            File[] listOfFiles = dir.listFiles();
            System.out.println(listOfFiles.length);
            for (File file : listOfFiles) {
                FileUtils.deleteDirectory(file);
            }
        } catch (IOException e) {
            System.out.println(e);
        }

    }

}