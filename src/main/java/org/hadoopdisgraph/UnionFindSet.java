package org.hadoopdisgraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UnionFindSet {
    HashMap<Integer, Integer> hmap;

    UnionFindSet() {
        this.hmap = new HashMap<Integer, Integer>();
    }

    private int getEnd(int i) {
        while (hmap.get(i) != null) {
            i = hmap.get(i);
        }

        return i;
    }

    public Boolean addEdge(Edge edge) {
        int m = getEnd(edge.u);
        int n = getEnd(edge.v);

        // System.out.println(m + " : " + n);

        if (m != n) {
            if (n < m)
                hmap.put(n, m);
            else
                hmap.put(m, n);
            return true;
        } else
            return false;
    }

    public HashMap<Integer, ArrayList<Integer>> getConnectedComponent(List<Edge> list) {

        list.forEach(e -> {
            addEdge(e);
        });

        // System.out.println(hmap);

        while (true) {
            int flag = 1;

            for (int e : hmap.keySet()) {
                int parent = hmap.get(e);

                if (hmap.get(parent) != null && parent != hmap.get(parent)) {
                    hmap.put(e, hmap.get(parent));
                    flag = 0;
                }

            }
            if (flag == 1)
                break;
        }

        HashMap<Integer, ArrayList<Integer>> ret = new HashMap<Integer, ArrayList<Integer>>();
        hmap.keySet().forEach(e -> {
            int v = hmap.get(e);
            ArrayList<Integer> list2 = ret.get(v);
            if (list2 == null) {
                list2 = new ArrayList<Integer>();
            }
            list2.add(e);

            ret.put(v, list2);
        });

        ret.keySet().forEach(v -> ret.get(v).add(v));

        return ret;

    }

}