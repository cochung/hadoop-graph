package org.hadoopdisgraph;

import java.util.*;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import java.util.Random;

public class JobEdgeSample {
    static float threshold;

    public static class EdgeMapper extends Mapper<LongWritable, Text, LongWritable, Text> {

        static Random rand;
        {
            rand = new Random(System.currentTimeMillis());
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            Configuration conf = context.getConfiguration();
            float p = rand.nextFloat();
            float threshold = Float.valueOf(conf.get("threshold"));
            if (p < threshold)
                context.write(key, value);
        }
    }

    public static class EdgeReducer extends Reducer<LongWritable, Text, NullWritable, Text> {
        NullWritable out = NullWritable.get();

        @Override
        protected void reduce(LongWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            Iterator<Text> valuesIt = values.iterator();
            while (valuesIt.hasNext()) {

                context.write(out, valuesIt.next());
            }

        }
    }

    public static int run(Configuration conf, String input, String output) {
        System.out.println("JobEdgeSample   " + input + "    " + output);

        try {
            long start = System.currentTimeMillis();
            Job job = new Job(conf);

            job.setJarByClass(JobEdgeSample.class);
            job.setJobName("JobEdgeSample");

            FileInputFormat.addInputPath(job, new Path(input));
            FileOutputFormat.setOutputPath(job, new Path(output));

            job.setMapOutputKeyClass(LongWritable.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            job.setMapperClass(JobEdgeSample.EdgeMapper.class);
            job.setReducerClass(JobEdgeSample.EdgeReducer.class);

            int ret = job.waitForCompletion(true) ? 0 : 1;

            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "edgeSample");

            return ret;
        } catch (Exception e) {
            System.out.println(e);
            return 0;

        }
    }

}
