package org.hadoopdisgraph;
public class ElapsedTime {

    public static void logElapsedTime(long start, long end, String name) {

        double elapsedMilliSeconds = end - start;
        double elapsedSeconds = elapsedMilliSeconds / 1000.0;
        String ret = "elapsedtime:" + name + ":" + elapsedSeconds;
        System.out.println(ret);
    }
}
