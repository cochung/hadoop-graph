package org.hadoopdisgraph;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

public class MaximalMatching {
    // private List<Integer> matchedIndex;
    HashSet<Integer> matchedIndex;  
    private List<Edge> matchedEdge;
    List<Edge> edgeList;

    MaximalMatching(List<Edge> edgeList) {
        this.edgeList = edgeList;
        this.matchedIndex = new HashSet<Integer>();
        this.matchedEdge = new ArrayList<Edge>();
        edgeList.forEach(e -> {
            boolean c = !matchedIndex.contains(e.u) && !matchedIndex.contains(e.v);
            if (c) {
                matchedIndex.add(e.u);
                matchedIndex.add(e.v);
                matchedEdge.add(e);
            }
        });
    }

    public HashSet<Integer> getMatchedIndex() {
        return this.matchedIndex;
    }

    public List<Edge> getMatchedEdge() {
        return this.matchedEdge;
    }
}
