package org.hadoopdisgraph;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;

import com.google.gson.Gson;

import org.apache.hadoop.conf.Configuration;

public class DistributedMaximalMatching {

    private String prefix;
    private int job_count;
    private List<Edge> matchedEdges;
    private HashSet<Integer> matchedIndex;

    public DistributedMaximalMatching(Configuration conf, int numV, long numE, int base, String input) {
        job_count = 1;
        matchedEdges = new ArrayList<Edge>();
        matchedIndex = new HashSet<Integer>();

        getMaximalMatching(conf, numV, numE, base, input);

    }

    public List<Edge> getMatchedEdges() {
        return matchedEdges;
    }

    public HashSet<Integer> getMatchedIndex() {
        return matchedIndex;
    }

    private void getMaximalMatching(Configuration conf, int numV, long numE, int base, String input) {
        prefix = input.split("\\.")[0];

        while (true) {

            String output = prefix + "." + job_count + ".count";
            long count = JobEdgeCount.runWrap(conf, input, output);
            job_count++;
            System.out.println("count: " + count);

            if (count <= base) {
                List<Edge> colEdges = new ArrayList<Edge>();
                List<String> edgeList = HdfsUtil.readFile(conf, input);
                edgeList.forEach(line -> {
                    String[] e = line.split(" ");
                    colEdges.add(new Edge(-1, Integer.valueOf(e[0]), Integer.valueOf(e[1]), -1));
                });

                MaximalMatching maximal = new MaximalMatching(colEdges);
                List<Edge> edges = maximal.getMatchedEdge();
                HashSet<Integer> indexList = maximal.getMatchedIndex();
                mergedMatched(edges, indexList);
                break;

            } else {
                System.out.println("numV:   " + numV);
                System.out.println("count:   " + count);
                float p = (float) base / (10 * count);
                p = Math.min(p, 1.0f);
                System.out.println("probability：   " + p);
                // p = 0.01f;
                conf.set("threshold", String.valueOf(p));

                output = prefix + "." + job_count + ".sample";
                JobEdgeSample.run(conf, input, output);
                job_count++;

                List<String> sampleEdgeList = HdfsUtil.readFile(conf, output);

                List<Edge> colEdges = new ArrayList<Edge>();
                sampleEdgeList.forEach(line -> {
                    String[] e = line.split(" ");
                    colEdges.add(new Edge(-1, Integer.valueOf(e[0]), Integer.valueOf(e[1]), -1));
                });

                System.out.println("sampledgeCount: " + colEdges.size());

                MaximalMatching maximal = new MaximalMatching(colEdges);
                List<Edge> edges = maximal.getMatchedEdge();
                HashSet<Integer> indexList = maximal.getMatchedIndex();
                mergedMatched(edges, indexList);

                System.out.println("edges:  " + edges.size());
                System.out.println("vertix:  " + indexList.size());


                System.out.println("indexList_length: " + indexList.size());
                Gson gson = new Gson();
                String jsonList = gson.toJson(indexList);
                System.out.println("length: " + jsonList.length());
                conf.set("indexList", jsonList);

                output = prefix + "." + job_count + ".filter";

                JobEdgeFilter.run(conf, input, output);
                input = output;
                job_count++;

            }
        }
    }

    private void mergedMatched(List<Edge> edges, HashSet<Integer> indexList) {
        this.matchedEdges.addAll(edges);
        this.matchedIndex.addAll(indexList);
    }

}
