package org.hadoopdisgraph;

import java.util.ArrayList;
import java.util.List;

import java.util.Collections;

public class Kruskal {
    List<Edge> edgeList;
    UnionFindSet ufset;

    public Kruskal(List<Edge> edgeList) {
        this.edgeList = edgeList;
        this.ufset = new UnionFindSet();
    }

    public List<Edge> getMST() {

        Collections.sort(edgeList);
        List<Edge> ret = new ArrayList<Edge>();

        for (Edge edge : this.edgeList) {
            Boolean bool = ufset.addEdge(edge);
            if (bool == true)
                ret.add(edge);
        }
        return ret;
    }

}
