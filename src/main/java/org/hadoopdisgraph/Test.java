package org.hadoopdisgraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.hadoop.conf.Configuration;

public class Test {

    public static void initMSTTestData(Configuration conf, int numV, long numE, int base, String filename) {

        try {
            long start = System.currentTimeMillis();
            // List<Edge> col = new ArrayList<Edge>();
            long count = 0;
            Random rand = new Random(System.currentTimeMillis());
            List<String> lines = new ArrayList<String>();
            while (count < numE) {
                int a = rand.nextInt(numV - 1) + 1;
                int b = rand.nextInt(numV - 1) + 1;
                int c = rand.nextInt(numV - 1) + 1;
                if (a != b) {
                    count++;
                    // Edge e = new Edge(count, a, b, c);
                    // col.add(e);
                    lines.add(count + " " + a + " " + b + " " + c);
                }
            }
            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "test_init");

            long start2 = System.currentTimeMillis();
            String username = System.getProperty("user.name");
            String rootPath = "/user/" + username;
            HdfsUtil.removeFile(conf, rootPath);
            HdfsUtil.writeFile(conf, filename, lines);

            long end2 = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start2, end2, "test_write");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void initMaximalTestData(Configuration conf, int numV, long numE, int base, String filename) {

        try {
            long start = System.currentTimeMillis();
            // List<Edge> col = new ArrayList<Edge>();
            long count = 0;
            Random rand = new Random(System.currentTimeMillis());
            List<String> lines = new ArrayList<String>();
            while (count < numE) {
                int a = rand.nextInt(numV - 1) + 1;
                int b = rand.nextInt(numV - 1) + 1;
                int c = -1;
                if (a != b) {
                    count++;
                    // Edge e = new Edge(-1, a, b, c);
                    // col.add(e);
                    lines.add(count + " " + a + " " + b + " " + c + " ");
                }
            }
            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "test_init");

            long start2 = System.currentTimeMillis();
            HdfsUtil.removeFile(conf, "input");
            HdfsUtil.writeFile(conf, filename, lines);

            long end2 = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start2, end2, "test_write");

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static void writeTestData() {
        // String readpath = "/home/cochung/spark-install/mst/input/input.2";
        // String writePath = "/home/cochung/spark-install/mst/input/input.3";
        // JavaRDD<String> readlines = sc.textFile(readpath);
        // JavaRDD<String> out = readlines.zipWithIndex().map(v -> v._2 + " " + v._1);
        // out.repartition(1).saveAsTextFile(writePath);
    }
}
