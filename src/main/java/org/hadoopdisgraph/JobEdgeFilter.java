package org.hadoopdisgraph;

import java.util.*;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.fs.Path;
import java.util.Random;
import org.apache.hadoop.conf.Configuration;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;

public class JobEdgeFilter {

    public static class EdgeMapper extends Mapper<LongWritable, Text, LongWritable, Text> {
        Random rand = new Random();
        static HashSet<Integer> indexList;

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            if (EdgeMapper.indexList == null) {
                System.out.println("null");
                Configuration conf = context.getConfiguration();
                String indexListStr = conf.get("indexList");
                Gson gson = new Gson();
                Type edgeListType = new TypeToken<HashSet<Integer>>() {
                }.getType();

                indexList = gson.fromJson(indexListStr, edgeListType);
            }
            String str = value.toString();
            String[] edge = str.split(" ");
            int u = Integer.valueOf(edge[1]);
            int v = Integer.valueOf(edge[2]);

            if (!indexList.contains(u) && !indexList.contains(v)) {

                context.write(key, value);
            }

        }
    }

    public static class EdgeReducer extends Reducer<LongWritable, Text, NullWritable, Text> {
        NullWritable out = NullWritable.get();

        @Override
        protected void reduce(LongWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            Iterator<Text> valuesIt = values.iterator();
            while (valuesIt.hasNext()) {

                context.write(out, valuesIt.next());
            }

        }
    }

    public static int run(Configuration conf, String input, String output) {

        System.out.println("JobEdgeFilter   " + input + "    " + output);

        try {
            long start = System.currentTimeMillis();

            Job job = new Job(conf);
            job.setJarByClass(JobEdgeFilter.class);
            job.setJobName("JobEdgeFilter");

            FileInputFormat.addInputPath(job, new Path(input));
            FileOutputFormat.setOutputPath(job, new Path(output));

            job.setMapOutputKeyClass(LongWritable.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(NullWritable.class);
            job.setOutputValueClass(Text.class);
            job.setOutputFormatClass(TextOutputFormat.class);

            job.setMapperClass(JobEdgeFilter.EdgeMapper.class);
            job.setReducerClass(JobEdgeFilter.EdgeReducer.class);

            int ret = job.waitForCompletion(true) ? 0 : 1;
            long end = System.currentTimeMillis();
            ElapsedTime.logElapsedTime(start, end, "edgeFilter");
            return ret;
        } catch (Exception e) {
            System.out.println(e);
            return 0;

        }
    }

}
