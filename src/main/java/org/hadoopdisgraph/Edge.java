package org.hadoopdisgraph;

 
import java.util.List;
import java.util.ArrayList;
import org.apache.hadoop.io.Text;
 

public class Edge implements  Comparable {
    long num;
    int u;
    int v;
    int weight;
    int partition = -1;//debug purpose

    Edge(long num, int u, int v, int weight) {
        this.num = num;
        this.u = u;
        this.v = v;
        this.weight = weight;
    }

    Edge(int partition, Edge e) {
        this.num = e.num;
        this.u = e.u;
        this.v = e.v;
        this.weight = e.weight;
        this.partition = partition;
    }

    // @Override
    public String toString3() {
        // String out = "num " + num + " u " + u + " v " + v + " weight " + weight + " partition " + partition;
        String out = u + " " + v + " " + weight;
        return out;
    }

    public String toString2() {
        // String out = "num " + num + " u " + u + " v " + v + " weight " + weight + " partition " + partition;
        String out = num + " " + u + " " + v + " " + weight;
        return out;
    }

    public static List<Edge> valueOf(Iterable<Text> values) {
        List<Edge> edges = new ArrayList<Edge>();
        for (Text value : values) {

            String str = value.toString();
            String[] edge = str.split(" ");

            long num = Long.valueOf(edge[0]);
            int u = Integer.valueOf(edge[1]);
            int v = Integer.valueOf(edge[2]);
            int weight = Integer.valueOf(edge[3]);
            edges.add(new Edge(num, u, v, weight));
        }
        return edges;
    }

    public static List<Edge> valueOf(List<String> lines) {
        List<Edge> list = new ArrayList<Edge>();
        for (String line : lines) {
            Edge e = Edge.valueOf(line);
            list.add(e);

        }
        return list;
    }

    public static Edge valueOf(String line) {
        String[] tmp = line.split(" ");
        long n = Long.valueOf(tmp[0]);
        int u = Integer.valueOf(tmp[1]);
        int v = Integer.valueOf(tmp[2]);
        int weight = Integer.valueOf(tmp[3]);
        Edge e = new Edge(n, u, v, weight);
        return e;
    }

    public static Edge valueOf2(String line) {
        String[] tmp = line.split(" ");

        int u = Integer.valueOf(tmp[0]);
        int v = Integer.valueOf(tmp[1]);
        int weight = Integer.valueOf(tmp[2]);
        Edge e = new Edge(-1, u, v, weight);
        return e;
    }

    @Override
    public int compareTo(Object e) {

        Edge e1 = (Edge) e;
        if (this.weight > e1.weight)
            return 1;
        else if (this.weight == e1.weight)
            return 0;
        else
            return -1;
    }

    

}